import matplotlib.pyplot as plt
import numpy as np
def Td(x):
    return -x+2
def T(x):
    return 2-(19/10)*np.e**(-x)

array = [0.1]
arrayx = [0]

N=10
eksp=0
h=0.5
y=0.1

while eksp < N:
    y = array[-1]+Td(y)*h
    array.append(y)
    arrayx.append(eksp)
    eksp+=h

array_implis = [0.1]
arrayx_implis = [0]

implis=0
y_implis=0.1

while implis < N:
    y_implis = array_implis[-1]+Td(array_implis[-1]+Td(y_implis)*h)*h
    array_implis.append(y_implis)
    arrayx_implis.append(implis)
    implis+=h


array_trapes = [0.1]
arrayx_trapes = [0]

trapes=0
y_trapes=0.1

while trapes < N:
    y_trapes = array_trapes[-1]+(((Td(array_trapes[-1]+Td(y_trapes)*h))+Td(y_trapes))/2)*h
    array_trapes.append(y_trapes)
    arrayx_trapes.append(trapes)
    trapes+=h

plt.plot(arrayx, array, 'r')
plt.plot(arrayx_implis, array_implis, 'g')
plt.plot(arrayx_trapes, array_trapes, 'b')
x_verdier= np.linspace(0, 10, 100)
y_verdier= T(x_verdier)
plt.plot(x_verdier, y_verdier, 'y')
plt.show()
